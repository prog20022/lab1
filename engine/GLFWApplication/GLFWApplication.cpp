#include <GLFWApplication.h>

void GLAPIENTRY
MessageCallback(
	GLenum source,
	GLenum type,
	GLenum id,
	GLenum severity,
	GLsizei length,

	const GLchar* message,
	const void* userParam)
{
	cerr << "GL CALLBACK:" << (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "") <<
		"type = 0x" << type <<
		", severity = 0x" << severity <<
		", message =" << message << "\n";
};

unsigned int GLFWApplication::parseArguments(int argc, char** argv) {

	//creating our command line attributes
	try {

		TCLAP::CmdLine cmd("", ' ', GLFWApplication::version, false);

		TCLAP::ValueArg<int> heightArg("h", "height", "Height of the window", false, 600, "int");
		cmd.add(heightArg);

		TCLAP::ValueArg<int> widthArg("w", "width", "Width of the window", false, 600, "int");
		cmd.add(widthArg);

		// Parse the argv array.
		cmd.parse(argc, argv);

		GLFWApplication::height = heightArg.getValue();
		GLFWApplication::width = widthArg.getValue();
		
		//ensuring positive values in the argument
		if (height < 1 || width < 1) {
			cout << "The height and width parameters have to be 1 or greater." << endl;
			cin.get();
			return EXIT_FAILURE;
		}
		
	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
	return EXIT_SUCCESS;
}

unsigned GLFWApplication::init() {

	int success;
	char infoLog[512];

	//initiating glfw, the library creating the window
	if (!glfwInit()) {

		cin.get();
		return EXIT_FAILURE;

	}

	//setting up the window
	glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//starting debug context
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	GLFWApplication::window = glfwCreateWindow(GLFWApplication::width, GLFWApplication::height, "Hello OpenGL", nullptr, nullptr);

	//making sure the window can be creeated
	if (GLFWApplication::window == nullptr) {
		cout << "Failed to create a GLFW window." << endl;
		glfwTerminate();
		cin.get();
		return EXIT_FAILURE;
	};

	glfwMakeContextCurrent(GLFWApplication::window);

	//loading openGL functions with the help of GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		cout << "Failed to initialize glad." << endl;
		glfwTerminate();
		cin.get();
		return EXIT_FAILURE;
	};

	//enable capture of debug output, if the debug context was successfully created
	int flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(MessageCallback, 0);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}

	return EXIT_SUCCESS;


}
