#ifndef GLFWAPPLICATION_H
#define GLFWAPPLICATION_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <tclap/CmdLine.h>

using namespace std;

//error callback function
void GLAPIENTRY
MessageCallback(
	GLenum source,
	GLenum type,
	GLenum id,
	GLenum severity,
	GLsizei length,

	const GLchar* message,
	const void* userParam);

class GLFWApplication {

protected:
	const char* name;
	string version;
	int height;
	int width;
	GLFWwindow* window;

public:
	GLFWApplication(const char* name, string version) {
		this->version = version;
		this->name = name;
	}
	virtual ~GLFWApplication(){};

	virtual unsigned int parseArguments(int argc, char** argv);
	virtual unsigned init();
	virtual unsigned run() = 0;
	
};

#endif


