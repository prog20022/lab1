#ifndef GEOMETRICTOOLS_H
#define GEOMETRICTOOLS_H

#include <array>

namespace GeometricTools {
	constexpr std::array<GLfloat, 3 * 2> UnitTriangle2D = { -0.5f, -0.5f, 0.5f, -0.5f, 0.0f, 0.5f };// [2,3]
	constexpr std::array<GLfloat, 4 * 2> UnitSquare2D = { 0.5f, 0.5f, 0.5f,-0.5f, -0.5f,-0.5f, -0.5f,0.5f }; //[2,4]
	constexpr std::array<GLfloat, 3 * 8> UnitCube = { -0.5f, 0.5f, 0.5f,   // 0
														0.5f,0.5f, 0.5f,   // 1
														-0.5f,-0.5f,0.5f,  // 2
														0.5f, -0.5f, 0.5f, // 3
														-0.5f, 0.5f, -0.5f,// 4
														0.5f,0.5f, -0.5f,  // 5
														-0.5f,-0.5f,-0.5f, // 6
														0.5f, -0.5f, -0.5f // 7 
																			};
	constexpr std::array<GLint, 6 * 6> UnitCubeTopology = {
																0, 1, 2, 2, 1, 3,
																4, 0, 6, 6, 0, 2,
																5, 1, 7, 7, 1, 3,
																4, 5, 6, 6, 5, 7,
																4, 5, 0, 0, 5, 1,
																6, 7, 2, 2, 7, 3
																					};

	constexpr std::array<GLfloat, 3 * 24> UnitCubeIllumination = { -0.5f, 0.5f, 0.5f,
																	0.5f,0.5f, 0.5f,
																	-0.5f,-0.5f,0.5f,
																	0.5f, -0.5f, 0.5f,

																	-0.5f, 0.5f, -0.5f,
																	-0.5f, 0.5f, 0.5f,
																	-0.5f,-0.5f,-0.5f,
																	-0.5f,-0.5f,0.5f,

																	0.5f, 0.5f, -0.5f,
																	0.5f, 0.5f, 0.5f,
																	0.5f, -0.5f, -0.5f,
																	0.5f, -0.5f, 0.5f,

																	-0.5f, 0.5f, -0.5f,
																	0.5f,0.5f, -0.5f,
																	-0.5f,-0.5f,-0.5f,
																	0.5f, -0.5f, -0.5f,

																	-0.5f, 0.5f, -0.5f,
																	0.5f,0.5f, -0.5f,
																	-0.5f, 0.5f, 0.5f,
																	0.5f,0.5f, 0.5f,

																	-0.5f,-0.5f,-0.5f,
																	0.5f, -0.5f, -0.5f,
																	-0.5f,-0.5f,0.5f,
																	0.5f, -0.5f, 0.5f
																							};


	constexpr std::array<GLint, 6 * 6> UnitCubeTopologyIllumination = {
																		0, 1, 2, 2, 1, 3,
																		4, 5, 6, 6, 5, 7,
																		8, 9, 10, 10, 9, 11,
																		12, 13, 14, 14, 13, 15,
																		16, 17, 18, 18, 17, 19,
																		20, 21, 22, 22, 21, 23
																								};

	constexpr std::array<GLfloat, 3 * 24> UnitCubeIlluminationNormals{
																		0.0f, 0.0f, 1.0f,
																		0.0f, 0.0f, 1.0f,
																		0.0f, 0.0f, 1.0f,
																		0.0f, 0.0f, 1.0f,

																		-1.0f, 0.0f, 0.0f,
																		-1.0f, 0.0f, 0.0f,
																		-1.0f, 0.0f, 0.0f,
																		-1.0f, 0.0f, 0.0f,

																		1.0f, 0.0f, 0.0f,
																		1.0f, 0.0f, 0.0f,
																		1.0f, 0.0f, 0.0f,
																		1.0f, 0.0f, 0.0f,

																		0.0f, 0.0f, -1.0f,
																		0.0f, 0.0f, -1.0f,
																		0.0f, 0.0f, -1.0f,
																		0.0f, 0.0f, -1.0f,

																		0.0f, 1.0f, 0.0f,
																		0.0f, 1.0f, 0.0f,
																		0.0f, 1.0f, 0.0f,
																		0.0f, 1.0f, 0.0f,

																		0.0f,-1.0f,0.0f,
																		0.0f,-1.0f,0.0f,
																		0.0f,-1.0f,0.0f,
																		0.0f,-1.0f,0.0f
																						 };
	
	template<int xDivs, int yDivs>
	constexpr std::array<float, ((xDivs * 2) * (yDivs * 2) * 2)> UnitGridGeometry2D() {

		int gridSize = ((xDivs * 2) * (yDivs * 2) * 2);
		std::array<float, ((xDivs * 2) * (yDivs * 2) * 2)> grid;

		float xDs = xDivs;
		float yDs = yDivs;

		//the formula under calculates the increment for columns
		//the total amount of increments are always the amount 
		//of divisions, and we divide by x divisions for columns
		float columnIncrement = 1.0f / xDs;
		//the starting position is always -0.5f
		float columnPos = -0.5f;

		int row = 0;
		int counter = 0;

		//iterate through every row
		for (int j = 0; j < yDivs * 2; j++){
			//iterate through every index of every row
			//we jump 2 because we just do the x coordinates here
			for (int i = 0; i < xDivs * 2 * 2; i += 2) {
				grid[row + i] = columnPos;
				//+1 increment after the first point
				if (i == 0) columnPos += columnIncrement;
				//otherwise +1 increment after every 2 points
				else {
					counter++;
					if (counter == 2) {
						columnPos += columnIncrement;
						counter = 0;
					}
				}
			}
			//jumping every row means jumping xDivs * 2 * 2 indexes
			row += xDivs * 2 * 2;
			//reset the initial position
			columnPos = -0.5f;
			//reset the counter
			counter = 0;
		}
		
		
		//the principle for the rows are the same as for the
		//columns
		float rowIncrement = 1.0f / yDs;
		float rowPos = 0.5f;

		row = 0;

		for (int j = 0; j < yDivs * 2; j++) {
			//start at 1 because working on y coordinates
			for (int i = 1; i < xDivs * 2 * 2; i += 2) {
				grid[row + i] = rowPos;
			}
			//we do the same as for x coordinates, but here
			//we do it on the y axis. And we go from top to bottom
			//hence doing -increment
			if (j == 0) rowPos -= rowIncrement;
			else {
				counter++;
				if (counter == 2) {
					rowPos -= rowIncrement;
					counter = 0;
				}
			}
			row += xDivs * 2 * 2;
		}

		return grid;
	}

	template<int xDivs, int yDivs>
	constexpr std::array<float, ((xDivs * 2)* (yDivs * 2) * 2)> UnitGridGeometry2DTexture() {

		int gridSize = ((xDivs * 2) * (yDivs * 2) * 2);
		std::array<float, ((xDivs * 2)* (yDivs * 2) * 2)> grid;

		float xDs = xDivs;
		float yDs = yDivs;

		//the formula under calculates the increment for columns
		//the total amount of increments are always the amount 
		//of divisions, and we divide by x divisions for columns
		float columnIncrement = 1.0f / xDs;
		//the starting position is always -0.5f
		float columnPos = 0.0f;

		int row = 0;
		int counter = 0;

		//iterate through every row
		for (int j = 0; j < yDivs * 2; j++) {
			//iterate through every index of every row
			//we jump 2 because we just do the x coordinates here
			for (int i = 0; i < xDivs * 2 * 2; i += 2) {
				grid[row + i] = columnPos;
				//+1 increment after the first point
				if (i == 0) columnPos += columnIncrement;
				//otherwise +1 increment after every 2 points
				else {
					counter++;
					if (counter == 2) {
						columnPos += columnIncrement;
						counter = 0;
					}
				}
			}
			//jumping every row means jumping xDivs * 2 * 2 indexes
			row += xDivs * 2 * 2;
			//reset the initial position
			columnPos = 0.0f;
			//reset the counter
			counter = 0;
		}


		//the principle for the rows are the same as for the
		//columns
		float rowIncrement = 1.0f / yDs;
		float rowPos = 1.0f;

		row = 0;

		for (int j = 0; j < yDivs * 2; j++) {
			//start at 1 because working on y coordinates
			for (int i = 1; i < xDivs * 2 * 2; i += 2) {
				grid[row + i] = rowPos;
			}
			//we do the same as for x coordinates, but here
			//we do it on the y axis. And we go from top to bottom
			//hence doing -increment
			if (j == 0) rowPos -= rowIncrement;
			else {
				counter++;
				if (counter == 2) {
					rowPos -= rowIncrement;
					counter = 0;
				}
			}
			row += xDivs * 2 * 2;
		}

		return grid;
	}

	template<int xDivs, int yDivs>
	constexpr std::array<int, xDivs* yDivs * 6> UnitGridTopologyTriangles() {

		std::array<int, xDivs* yDivs * 6> topology;

		int vertices = 0;

		int triangles = 0;

		for (int j = 0; j < yDivs; j++) {

			for (int i = 0; i < xDivs; i++) {

				topology[triangles + 0] = vertices + 0;
				topology[triangles + 1] = vertices + 1;
				topology[triangles + 2] = vertices + xDivs * 2;
				topology[triangles + 3] = vertices + xDivs * 2;
				topology[triangles + 4] = vertices + 1;
				topology[triangles + 5] = vertices + (xDivs * 2) + 1 ;


				triangles += 6;
				//because vertices are doubled, at every loop
				//it jumps 2 vertices
				vertices += 2;
			}
			//at the end of every row, the jump
			//is a set of points, also because row points are doubled
			vertices += (xDivs * 2);
		}

		return topology;
	}

	//this function creates a chessboard style color setup
	template<int xDivs, int yDivs>
	constexpr std::array<float, (xDivs * 2) * (yDivs * 2) * 3> chessBoardColorSetup() {

		std::array<float, (xDivs * 2) * (yDivs * 2) * 3 > color;

		int vertex = 0;
		bool black = true;

		//iterates through all the 'points' of the grid
		for (int i = 0; i < yDivs * 2 ; i++) {
			for (int j = 0; j < xDivs * 2 ; j++) {
				if (black) {
					color[vertex + 0] = 0.0f;
					color[vertex + 1] = 0.0f;
					color[vertex + 2] = 0.0f;
				}
				else {
					color[vertex + 0] = 1.0f;
					color[vertex + 1] = 1.0f;
					color[vertex + 2] = 1.0f;
				}
				//we jump 3 indexes at every loop
				//because 1 'point' is 3 indexes
				vertex += 3;
				if (xDivs % 2 == 1) {
					//if number of xDivisions is odd, we change color
					//at every 2 points, not 0, and not the last
					//point of a row.
					if ((j + 1) % 2 == 0 && (j + 1) != xDivs * 2) black = !black;
				}
				else {
					//if the number of divisions is even
					//we change color at every 2 point, but not
					//the first one
					if ((j + 1) % 2 == 0) black = !black;
				}
			}
			//we change color at every 2 rows of vertices,
			//keeping in mind that vertices inside the board
			//are doubled
			if ((i + 1) % 2 == 0 ) black = !black;
		}
		
		return color;

	}


}

#endif