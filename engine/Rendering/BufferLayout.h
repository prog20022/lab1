#ifndef BUFFERLAYOUT_H
#define BUFFERLAYOUT_H

#include <string>
#include <vector>
#include <ShadersDataTypes.h>

struct BufferAttribute {

	std::string name;
	ShaderDataType type;
	GLuint size;
	GLuint count;
	GLuint offset;
	GLboolean normalized;

	BufferAttribute(ShaderDataType _type, unsigned int _offset, const std::string &_name,
					GLboolean _normalized = false)
		//initializer list
		: name(_name), type(_type), count(ShaderDataTypeComponentCount(_type)), size(ShaderDataTypeSize(_type)),
		  offset(_offset), normalized(_normalized) {}


};

class BufferLayout {

private:
	std::vector<BufferAttribute> attributes;
	//GLsizei stride;

	//void calculateOffsetAndStride() {

		//GLsizei offset = 0;
		//stride = 0;

		/*
		for (auto& attribute : attributes) {
			attribute.offset = offset;
			offset += attribute.size;
			stride += attribute.size;
		}
		*/

	//}

public:
	BufferLayout() {}
	BufferLayout(const std::initializer_list<BufferAttribute>& _attributes)
		: attributes(_attributes)
	{
		//calculateOffsetAndStride();
	}

	const std::vector<BufferAttribute>& getAttributes() const { return attributes; }
	//GLsizei getStride() const { return stride; }

	std::vector<BufferAttribute>::iterator begin() { return attributes.begin(); }
	std::vector<BufferAttribute>::iterator end() { return attributes.end(); }
	std::vector<BufferAttribute>::const_iterator begin() const { return attributes.begin(); }
	std::vector<BufferAttribute>::const_iterator end() const { return attributes.end(); }

};

#endif