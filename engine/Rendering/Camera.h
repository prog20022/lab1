#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/fwd.hpp>

class Camera {

protected:
	glm::mat4 projectionMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = glm::mat4(1.0f);
	glm::mat4 viewProjectionMatrix = glm::mat4(1.0f);
	glm::vec3 position = glm::vec3(0.0f);

	Camera(const Camera& camera) {
		this->projectionMatrix = camera.projectionMatrix;
		this->viewMatrix = camera.viewMatrix;
		this->position = camera.position;
		this->viewProjectionMatrix = camera.viewProjectionMatrix;
	}

	virtual void recalculateMatrix() = 0;

public:
	Camera() = default;
	~Camera() = default;

	const glm::mat4& getProjectionMatrix() const {
		return this->projectionMatrix;
	}

	const glm::mat4& getViewMatrix() const {
		return this->viewMatrix;
	}

	const glm::mat4& getViewProjectionMatrix() const {
		return this->viewProjectionMatrix;
	}

	const glm::vec3& getPosition() const {
		return this->position;
	}

	void setPosition(const glm::vec3& pos) {
		this->position = pos; this->recalculateMatrix();
	}


};

#endif
