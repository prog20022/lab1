#include <OrthographicCamera.h>
#include <glm/gtc/matrix_transform.hpp>

OrthographicCamera::OrthographicCamera(const Frustrum& frustrum,
									   const glm::vec3& position, float rotation) {

	this->cameraFrustrum = frustrum;
	this->position = position;
	this->rotation = rotation;

	this->recalculateMatrix();


}

void OrthographicCamera::recalculateMatrix() {

	this->projectionMatrix = glm::ortho(cameraFrustrum.left, cameraFrustrum.right, 
										cameraFrustrum.bottom, cameraFrustrum.top, 
										cameraFrustrum.near, cameraFrustrum.far);

	this->viewMatrix = glm::translate(glm::mat4(1.0f), this->position) *
					   glm::rotate(glm::mat4(1.0f), glm::radians(this->rotation), glm::vec3(0.0f, 0.0f, 1.0f));

	this->viewProjectionMatrix =  this->projectionMatrix * this->viewMatrix;

}