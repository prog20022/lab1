#ifndef ORTHOGRAPHICCAMERA_H
#define ORTHOGRAPHICCAMERA_H

#include <Camera.h>
#include <glm/fwd.hpp>
#include <array>

class OrthographicCamera : public Camera {
public:
	
	struct Frustrum {
		float left;
		float right;
		float bottom;
		float top;
		float near;
		float far;
	};

	OrthographicCamera(const Frustrum& frustrum = { -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f },
					   const glm::vec3& position = glm::vec3(0.0f), float rotation = 0.0f);

	OrthographicCamera() = default;

	OrthographicCamera(const OrthographicCamera& camera) : Camera(camera) {
		this->rotation = camera.rotation;
		this->cameraFrustrum = camera.cameraFrustrum;
	}

	void setRotation(float rotation) {
		this->rotation = rotation; this->recalculateMatrix();
	}

	void setFrustrum(const Frustrum& frustrum) {
		this->cameraFrustrum = frustrum; this->recalculateMatrix();
	}

protected:

	float rotation;
	Frustrum cameraFrustrum;
	void recalculateMatrix();

};

#endif