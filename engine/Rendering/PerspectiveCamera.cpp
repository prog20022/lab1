#include <PerspectiveCamera.h>
#include <glm/gtc/matrix_transform.hpp>

PerspectiveCamera::PerspectiveCamera(const Frustrum& frustrum,
									 const glm::vec3& position,
									 const glm::vec3& lookAt,
									 const glm::vec3& upVector) 
{

	this->cameraFrustrum = frustrum;
	this->lookAt = lookAt;
	this->upVector = upVector;
	this->position = position;

	this->recalculateMatrix();

}

void PerspectiveCamera::recalculateMatrix() {

	this->projectionMatrix = glm::perspective(glm::radians(cameraFrustrum.angle),
											  cameraFrustrum.width / cameraFrustrum.height,
		                                      cameraFrustrum.near, cameraFrustrum.far);
	this->viewMatrix = glm::lookAt(this->position, this->lookAt, this->upVector);
	this->viewProjectionMatrix = this->projectionMatrix * this->viewMatrix;

}