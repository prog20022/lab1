#ifndef PERSPECTIVECAMERA_H
#define PERSPECTIVECAMERA_H

#include <Camera.h>
#include <array>

class PerspectiveCamera : public Camera {
public:
	struct Frustrum {
		float angle;
		float width;
		float height;
		float near;
		float far;
	};

	PerspectiveCamera(const Frustrum& frustrum = { 45.0f, 1.0f, 1.0f, 1.0f, -10.0f },
					  const glm::vec3& position = glm::vec3(0.0f),
					  const glm::vec3& lookAt = glm::vec3(-1.0f),
					  const glm::vec3& upVector = glm::vec3(0.0f, 1.0f, 0.0f));
	~PerspectiveCamera() = default;
	PerspectiveCamera(const PerspectiveCamera& camera) : Camera(camera) {
		this->lookAt = camera.lookAt;
		this->upVector = camera.upVector;
		this->cameraFrustrum = camera.cameraFrustrum;
	};

	void setFrustrum(const Frustrum& frustrum) {
		this->cameraFrustrum = frustrum; this->recalculateMatrix();
	}

	void setLookAt(const glm::vec3& lookAt) {
		this->lookAt = lookAt; this->recalculateMatrix();
	}

	void setUpVector(const glm::vec3& upVector) {
		this->upVector = upVector; this->recalculateMatrix();
	}

protected:
	void recalculateMatrix();

	glm::vec3 lookAt;
	glm::vec3 upVector;
	Frustrum cameraFrustrum;


};

#endif