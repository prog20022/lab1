#ifndef RENDERCOMMANDS_H
#define RENDERCOMMANDS_H

namespace RenderCommands {
	inline void clear(GLuint mode = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) { glClear(mode); }
	inline void setPolygonMode(GLenum face, GLenum mode) { glPolygonMode(face, mode); }
	inline void setWireframeMode() { glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); }
	inline void setSolidMode() { glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); }
	inline void drawIndex(const std::shared_ptr<VertexArray>& vao, GLenum primitive) { glDrawElements(primitive, vao->getIndexBuffer()->getCount(), GL_UNSIGNED_INT, nullptr); }
	inline void setClearColor(glm::vec4 color = glm::vec4(0.5f, 0.0f, 0.0f, 1.0f)){ glClearColor(color[0], color[1], color[2], color[3]); }
}

#endif