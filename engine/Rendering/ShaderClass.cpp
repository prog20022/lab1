#include <ShaderClass.h>
#include <iostream>

unsigned int Shader::compileShader(GLenum shaderType, const std::string shaderSrc) {

	unsigned int id = glCreateShader(shaderType);
	const char* src = shaderSrc.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (!result) {

		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::cout << "Failed to compile " <<
			(shaderType == GL_VERTEX_SHADER ? "vertex" : "fragment")
			<< "shader!" << std::endl;
		std::cout << message << std::endl;
		glDeleteShader(id);
		return 0;
	}

	return id;

}

Shader::Shader(const std::string& vertexSrc, const std::string& fragmentSrc) {

	Shader::shaderProgram = glCreateProgram();
	Shader::vertexShader = compileShader(GL_VERTEX_SHADER, vertexSrc);
	Shader::fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentSrc);

	glAttachShader(Shader::shaderProgram, Shader::vertexShader);
	glAttachShader(Shader::shaderProgram, Shader::fragmentShader);
	glLinkProgram(Shader::shaderProgram);
	glValidateProgram(Shader::shaderProgram);

	glDeleteShader(Shader::vertexShader);
	glDeleteShader(Shader::fragmentShader);

}

Shader::~Shader() {

}

void Shader::bind() const {
	glUseProgram(Shader::shaderProgram);
}
void Shader::unbind() const {
	glUseProgram(0);
}

void Shader::uploadUniformFloat1(const std::string& name, const float& value) {

	int location = glGetUniformLocation(Shader::shaderProgram, name.c_str());

	if (location != -1) {
		bind();
		glUniform1f(location, value);
	}
	else {
		std::cout << "\n\n **ERROR** " << name << " does not exist in the shader.\n\n";
	}

}

void Shader::uploadUniformInt1(const std::string& name, const int& value) {

	int texture = glGetUniformLocation(Shader::shaderProgram, name.c_str());

	if (texture != -1) {
		bind();
		glUniform1i(texture, value);
	}
	else {
		std::cout << "\n\n **ERROR** " << name << " does not exist in the shader.\n\n";
	}

}

void Shader::uploadUniformFloat3(const std::string& name, const glm::vec3& vector) {

	int vertexColorLocation = glGetUniformLocation(Shader::shaderProgram, name.c_str());

	if (vertexColorLocation != -1) {
		bind();
		glUniform3f(vertexColorLocation, vector.x, vector.y, vector.z);
	}
	else {
		std::cout << "\n\n **ERROR** " << name << " does not exist in the shader.\n\n";
	}

}

void Shader::uploadUniformMat4(const std::string& name, const glm::mat4& matrix) {

	int viewMatrix = glGetUniformLocation(Shader::shaderProgram, name.c_str());

	if (viewMatrix != -1) {
		bind();
		glUniformMatrix4fv(viewMatrix, 1, GL_FALSE, glm::value_ptr(matrix));
	}
	else {
		std::cout << "\n\n **ERROR** " << name << " does not exist in the shader.\n\n";
	}


}