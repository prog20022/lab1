#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
private:
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint shaderProgram;

    static unsigned int compileShader(GLenum shaderType, const std::string shaderSrc);

public:
    Shader(const std::string& vertexSrc, const std::string& fragmentSrc);
    ~Shader();

    void bind() const;
    void unbind() const;
    void uploadUniformFloat1(const std::string& name, const float& value);
    void uploadUniformInt1(const std::string& name, const int& value);
    void uploadUniformFloat3(const std::string& name, const glm::vec3& vector);
    void uploadUniformMat4(const std::string& name, const glm::mat4& vector);
};

#endif