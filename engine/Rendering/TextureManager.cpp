#include <stb_image.h>
#include <TextureManager.h>
#include <iostream>

bool TextureManager::loadTexture2DRGBA(const std::string& name, const std::string& filePath, GLuint unit, bool mipMap) {

	int width, height, bpp;

	auto data = this->loadTextureImage(filePath, width, height, bpp, STBI_rgb_alpha);

	if (!data) {
		return false;
	}

	GLuint tex;
	glGenTextures(1, &tex);
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	if (mipMap) {
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	Texture texture;
	texture.mipMap = mipMap;
	texture.width = width;
	texture.height = height;
	texture.name = name;
	texture.filePath = filePath;
	texture.unit = unit;
	texture.type = texture2D;

	this->textures.push_back(texture);

	this->freeTextureImage(data);

	return true;

}	

bool TextureManager::loadCubeMapRGBA(const std::string& name, const std::string& filePath, GLuint unit, bool mipMap) {
	int width, height, bpp;

	auto data = this->loadTextureImage(filePath, width, height, bpp, STBI_rgb_alpha);

	if (!data) {
		return false;
	}

	GLuint tex;
	glGenTextures(1, &tex);
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

	for (int i = 0; i < 6; i++) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}

	if (mipMap) {
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	Texture texture;
	texture.mipMap = mipMap;
	texture.width = width;
	texture.height = height;
	texture.name = name;
	texture.filePath = filePath;
	texture.unit = unit;
	texture.type = cubeMap;

	this->textures.push_back(texture);
	this->freeTextureImage(data);

	return true;
}

GLuint TextureManager::getUnitByName(const std::string& name) const {

	for (const auto& texture : this->textures) {
		if (!texture.name.compare(name)) {
			return texture.unit;
		}
	}
	return -1;
}

unsigned char* TextureManager::loadTextureImage(const std::string& filePath, int& width, int& height, int& bpp, int format) const {

	return stbi_load(filePath.c_str(), &width, &height, &bpp, format);

}

void TextureManager::freeTextureImage(unsigned char* data) const {

	if (data) {
		stbi_image_free(data);
	}

}
