#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <glad/glad.h>
#include <string>
#include <vector>

class TextureManager {

public:
	enum textureType { texture2D, texture3D, cubeMap, skyBox };

	struct Texture {
		bool mipMap;
		int width, height, bpp;
		std::string name;
		std::string filePath;
		GLuint unit;
		TextureManager::textureType type;
	};

	static TextureManager* getInstace() {
		return TextureManager::instance != nullptr ? TextureManager::instance : TextureManager::instance = new TextureManager();
	}

	bool loadTexture2DRGBA(const std::string& name, const std::string& filePath, GLuint unit, bool mipMap = true);
	bool loadCubeMapRGBA(const std::string& name, const std::string& filePath, GLuint unit, bool mipMap = true);
	GLuint getUnitByName(const std::string& name) const;

private:
	unsigned char* loadTextureImage(const std::string& filePath, int& width, int& height, int& bpp, int format) const;
	void freeTextureImage(unsigned char* data) const;

	TextureManager() {};
	~TextureManager(){};
	TextureManager(const TextureManager&) = delete;
	void operator=(const TextureManager&) = delete;

	inline static TextureManager* instance = nullptr;
	std::vector<TextureManager::Texture> textures;
};

#endif