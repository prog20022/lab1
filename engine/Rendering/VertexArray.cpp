#include <VertexArray.h>


VertexArray::VertexArray() {

	glGenVertexArrays(1, &m_vertexArrayID);
}

VertexArray::~VertexArray() {

	glDeleteVertexArrays(1, &m_vertexArrayID);

}

void VertexArray::bind() const {

	glBindVertexArray(m_vertexArrayID);

}

void VertexArray::unbind() const {

	glBindVertexArray(0);

}

void VertexArray::addVertexBuffer(const std::shared_ptr<VertexBuffer>& _vertexBuffer) {
	
	bind();
	_vertexBuffer->bind();
	const BufferLayout &elements = _vertexBuffer->getLayout();

	int i = 0;
	for (const auto& element : elements) {
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, element.count, ShaderDataTypeToOpenGLBaseType(element.type),
			element.normalized, 0, (const void*)element.offset);
		i++;
	}
	
};

void VertexArray::setIndexBuffer(const std::shared_ptr<IndexBuffer>& _indexBuffer) {

	idxBuffer = _indexBuffer;
	idxBuffer->bind();

}