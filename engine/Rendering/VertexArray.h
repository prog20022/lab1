#ifndef VERTEXARRAY_H
#define VERTEXARRAY_H

#include <VertexBuffer.h>
#include <IndexBuffer.h>
#include <vector>
#include <memory>


class VertexArray {

private:
	GLuint m_vertexArrayID;
	std::vector<std::shared_ptr<VertexBuffer>> vertexBuffers;
	std::shared_ptr<IndexBuffer> idxBuffer;

	const std::vector<std::shared_ptr<VertexBuffer>>& getVertexBuffers() const { return vertexBuffers; }

public:
	VertexArray();
	~VertexArray();

	void bind() const;

	void unbind() const;

	void addVertexBuffer(const std::shared_ptr<VertexBuffer>& _vertexBuffer);

	void setIndexBuffer(const std::shared_ptr<IndexBuffer>& _indexBuffer);

	const std::shared_ptr<IndexBuffer>& getIndexBuffer() const { return idxBuffer; }


};

#endif