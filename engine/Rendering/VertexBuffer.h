#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <glad/glad.h>
#include <BufferLayout.h>

class VertexBuffer {

private:
	unsigned int m_RendererID;
	BufferLayout layout;
public:
	VertexBuffer(const void* data, unsigned int size);
	~VertexBuffer();

	const BufferLayout& getLayout() const { return layout; }
	void setLayout(const BufferLayout& _layout) { layout = _layout; }

	void bind() const;
	void unbind() const;

	void BufferSubData(GLintptr offset, GLsizeiptr size, const void* data) const;

};

#endif