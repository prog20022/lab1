#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>




using namespace std;

//error callback function
void GLAPIENTRY
MessageCallback(
	GLenum source,
	GLenum type,
	GLenum id,
	GLenum severity,
	GLsizei length,

	const GLchar* message,
	const void* userParam)
{
	cerr << "GL CALLBACK:" << (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "") <<
		"type = 0x" << type <<
		", severity = 0x" << severity <<
		", message =" << message << "\n";
}

int main(int argc, char* argv[]) {

	//variables used in the command line parsing but they are initialized outside of the exception catch
	//because they will be reused outside of the exception catch, further down the code
	int width, height;
	bool triangleB, squareB;

	//debug variables
	int success;
	char infoLog[512];

	//creating our command line attributes
	try {

		TCLAP::CmdLine cmd("", ' ', "1", false);

		TCLAP::ValueArg<int> heightArg("h", "height", "Height of the window", false, 600, "int");
		cmd.add(heightArg);

		TCLAP::ValueArg<int> widthArg("w", "width", "Width of the window", false, 600, "int");
		cmd.add(widthArg);

		TCLAP::SwitchArg triangleSwitch("t", "triangle", "Draws a triangle on the screen", cmd, false);
		TCLAP::SwitchArg squareSwitch("s", "square", "Draws a square on the screen", cmd, false);

		// Parse the argv array.
		cmd.parse(argc, argv);

		height = heightArg.getValue();
		width = widthArg.getValue();
		squareB = squareSwitch.getValue();
		triangleB = triangleSwitch.getValue();

		//ensuring positive values in the argument
		if (height < 1 || width < 1) {
			cout << "The height and width parameters have to be 1 or greater." << endl;
			cin.get();
			return EXIT_FAILURE;
		}

	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
	

	//initiating glfw, the library creating the window
	if (!glfwInit()) {

		cin.get();
		return EXIT_FAILURE;

	}

	//setting up the window
	glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//starting debug context
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	auto window = glfwCreateWindow(width, height, "HelloOpenGL", nullptr, nullptr);

	//making sure the window can be creeated
	if (window == nullptr) {
		cout << "Failed to create a GLFW window." << endl;
		glfwTerminate();
		cin.get();
		return EXIT_FAILURE;
	};

	glfwMakeContextCurrent(window);

	//loading openGL functions with the help of GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		cout << "Failed to initialize glad." << endl;
		glfwTerminate();
		cin.get();
		return EXIT_FAILURE;
	};

	//enable capture of debug output, if the debug context was successfully created
	int flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(MessageCallback, 0);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}

	//creating the vertices data of our triangle
	//3 vertices with 2 values each.
	//and the color of each vertices at the end
	GLfloat triangle[(3 * 2) + (3 * 3)] = {
		-0.5f, -0.5f, //bottom left
		0.5f,-0.5f, //bottom right
		0.0f,0.5f,   //top
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};
	

	//creating the vertices data of our square
	//4 vertices with 2 values each.
	//and the color of each vertices at the end
	
	GLfloat square[(4 * 2) + (3 * 4)] = {
		 0.5f, 0.5f, //top right
		 0.5f,-0.5f, //bottom right
		-0.5f,-0.5f, //bottom left
		 -0.5f,0.5f, //top left
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f
	};
	

	//an element array is used to draw the square
	GLuint indices[2 * 3] = {
		0,1,2,
		3,0,2
	};

	//creating a vertex array object to hold the data of the triangle and the sqaure
	GLuint vertexArrayId[2];
	glGenVertexArrays(2, vertexArrayId);

	//creating a buffer object to transfer the triangle and square data from the CPU to the GPU
	GLuint vertexBufferId[2];
	glGenBuffers(2, vertexBufferId);

	GLuint elementBufferObject;
	glGenBuffers(1, &elementBufferObject);

	//putting the triangle data into the 1st buffer object
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_DYNAMIC_DRAW);

	//indicating the layout of the bound buffer for the position of the triangle
	glBindVertexArray(vertexArrayId[0]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, nullptr);
	//enabling the specified attributes
	glEnableVertexAttribArray(0);

	//indicating the layout of the bound buffer for the color of the triangle
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)(2 * 3 * (sizeof(float))));
	glEnableVertexAttribArray(1);

	//putting the square data into the 2nd buffer object
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square), square, GL_DYNAMIC_DRAW);

	//putting the element data for the square into the element array buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//indicating the layout of the bound buffer for the position of the square
	glBindVertexArray(vertexArrayId[1]);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, nullptr);
	//enabling the specified attributes
	glEnableVertexAttribArray(0);

	//indicating the layout of the bound buffer for the color of the square
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)(2 * 4 * (sizeof(float))));
	glEnableVertexAttribArray(1);

	//the code for the vertex shader (vertex shader is creating the shape in the GPU)
	//and passing on the color data
	const string vertexShaderSrc = R"(
	#version 460 core

	layout(location = 0) in vec2 position;
	layout(location = 1) in vec3 aColor;

	out vec4 ourColor;

	void main(){
	gl_Position = vec4(position, 0.0, 1.0);
	ourColor = vec4(aColor, 1.0);
	}
	)";

	//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
	const string fragmentShaderSrc = R"(
	#version 460 core
	in vec4 ourColor;	

	out vec4 color;
	void main(){
	color = ourColor;
	}
	)";

	//creating the shader object
	auto vertexShader = glCreateShader(GL_VERTEX_SHADER);
	//turning the string of the source code into a char array
	const GLchar* vss = vertexShaderSrc.c_str();
	//loading the sourcecode into the shader object
	glShaderSource(vertexShader, 1, &vss, nullptr);
	//compiling the source into the shader object
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << endl;
	};

	//same process as above, but for the fragment shader
	auto fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fss = fragmentShaderSrc.c_str();
	glShaderSource(fragmentShader, 1, &fss, nullptr);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << endl;
	};

	//creatin a program combining the two shader objects
	auto shaderProgram = glCreateProgram();
	//attaching the vertex shader to the program
	glAttachShader(shaderProgram, vertexShader);
	//attache the fragment shader to the program
	glAttachShader(shaderProgram, fragmentShader);
	//connecting the vertex shader and fragment shader together, among other things
	glLinkProgram(shaderProgram);
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << endl;
	}

	//shaders object can be deleted once linked together in a program
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	//activating the program
	glUseProgram(shaderProgram);

	//sets background color
	glClearColor(0.5f, 0.0f, 0.0f, 1.0f);

	//needed to calculate elapsed time
	float lastFrame = 0;

	//variables needed to calculate the oscillation
	float delay = 0;
	float delay2 = 0;
	float delay3 = 0;

	while (!glfwWindowShouldClose(window)) {

		//absolutely necessary for the window to be responsive
		glfwPollEvents();

		//clears the window at every frame before rendering
		glClear(GL_COLOR_BUFFER_BIT);

		//getting the time elapsed
		//ensuring it can run indefinitely by
		//resetting theclock every 10 seconds;
		float currentFrame = glfwGetTime();
		float deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		if (currentFrame >= 10.0f) {
			glfwSetTime(0);
			lastFrame = 0;
		}

		//variables to calculate the oscillations
		float sign = 1;
		float sign2 = 1;
		float sign3 = 1;
		delay += deltaTime;
		delay2 += deltaTime;
		delay3 += deltaTime;
		float time = 1.0f;
		float time2 = 6.0f;
		float time3 = 3.0f;
		float PI = 3.14159265359;
		float amplitude = 0.5f;
		float angleTotal = 0.0f;
		float angleTotal2 = 0.0f;
		float angleTotal3 = 0.0f;
		float increment = 2 * PI / time * delay * sign;
		float increment2 = 2 * PI / time2 * delay2 * sign2;
		float increment3 = 2 * PI / time3 * delay3 * sign3;
		//this sets back the time to 0 after a full turn
		//to ensure the program can run indefinitely
		if (delay >= time) {
			delay = 0;
			sign = -sign;
		}
		if (delay2 >= time2) {
			delay2 = 0;
			sign2 = -sign2;
		}
		if (delay3 >= time3) {
			delay3 = 0;
			sign3 = -sign3;
		}

		//all these formulas give values between 0.0f and 1.0f and start at 0.5f
		//but their oscillation speed is different and one of them is a sin and the 
		//two others are cos, which makes for different oscillations
		float newColor = (amplitude * cos(angleTotal += increment)) + 0.5F;
		float newColor2 = (amplitude * sin(angleTotal2 += increment2)) + 0.5F;
		float newColor3 = (amplitude * cos(angleTotal3 += increment3)) + 0.5F;

		//the array used to update the color of the triangle
		GLfloat newColorTriangle[3 * 3]{
			0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f
		};

		//various oscillations given to different color data points of the triangle
		newColorTriangle[2] = newColor;
		newColorTriangle[4] = newColor;
		newColorTriangle[8] = newColor;
		newColorTriangle[0] = newColor2;
		newColorTriangle[6] = newColor2;
		newColorTriangle[7] = newColor2;
		newColorTriangle[1] = newColor3;
		newColorTriangle[3] = newColor3;
		newColorTriangle[5] = newColor3;

		//the array used to update the color of the square
		GLfloat newColorSquare[4 * 3]{
			0.2f, 0.0f, 0.2f,
			0.8f, 0.0f, 0.8f,
			0.3f, 0.0f, 0.3f,
			0.1f, 0.0f, 0.1f
		};

		//oscillates the green part of the square
		newColorSquare[1] = newColor2;
		newColorSquare[4] = newColor2;
		newColorSquare[7] = newColor2;
		newColorSquare[10] = newColor2;

		GLfloat newColorBackground[2]{
			0.0f, 0.0f
		};

		//the oscillation data for the background color
		newColorBackground[0] = newColor3;
		newColorBackground[1] = newColor;

		//oscillating the green and blue color of the background
		glClearColor(0.5f, newColorBackground[0], newColorBackground[1], 1.0f);

		//square is always drawn first, if both the square and triangle flag are used
		if (squareB) {
			glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]);
			//updates the color of the square
			glBufferSubData(GL_ARRAY_BUFFER, (8 * (sizeof(float))), 12 * (sizeof(float)), &newColorSquare);
			glBindVertexArray(vertexArrayId[1]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0); //(void*)(3*sizeof(int)));
		}

		if (triangleB) {
			glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]);
			//updates the color of the triangle
			glBufferSubData(GL_ARRAY_BUFFER, (6 * (sizeof(float))), 9 * (sizeof(float)), &newColorTriangle);
			glBindVertexArray(vertexArrayId[0]);
			glDrawArrays(GL_TRIANGLES, 0, 3);
		}

		//renders the window
		glfwSwapBuffers(window);

		//closes the window if esc key is pressed
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) break;

	}
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDeleteBuffers(2, vertexBufferId);
	glDeleteVertexArrays(2, vertexArrayId);
	glfwDestroyWindow(window);
	glfwTerminate();
	return EXIT_SUCCESS;
}
