#include "Lab2Application.h"
#include "Shaders.h"
#include <algorithm>
#include <iostream>
#include <string>

/*
*
* This program here draws a chessboard. 
* 
* This program uses a separate draw call for each square of the chess board
* 
* This is not very efficient, but it worked and at this level, performance
* is not as important
* 
*/



//this global variable is where in the grid that the
//tile selector is position. On which square it is
//it is a global variable so that the key_input function can update
//it and hence move the tile selector on the screen
int gGridRow = 4;
int gGridColumn = 4;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

array<float, 768> greenSelector(array<float, 768> color);

unsigned int Lab2Application::parseArguments(int argc, char** argv) {
	
	//creating our command line attributes
	try {
		
		TCLAP::CmdLine cmd("", ' ', GLFWApplication::version, false);
		
		TCLAP::ValueArg<int> heightArg("h", "height", "Height of the window", false, 600, "int");
		cmd.add(heightArg);

		TCLAP::ValueArg<int> widthArg("w", "width", "Width of the window", false, 600, "int");
		cmd.add(widthArg);
		
		// Parse the argv array.
		cmd.parse(argc, argv);

		Lab2Application::height = heightArg.getValue();
		Lab2Application::width = widthArg.getValue();

		//ensuring positive values in the argument
		if (height < 1 || width < 1) {
			cout << "The height and width parameters have to be 1 or greater." << endl;
			cin.get();
			return EXIT_FAILURE;
		}
		
	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}

	return EXIT_SUCCESS;
};


unsigned Lab2Application::run() {

	//we create our grid vertices
	std::array<float, 512> gridGeometry = GeometricTools::UnitGridGeometry2D<8, 8>();
	
	std::array<float, 768> boardColor = GeometricTools::chessBoardColorSetup<8, 8>();

	std::array<float, gridGeometry.size() + boardColor.size()> gridGeometryColor;
	std::copy(boardColor.begin(), boardColor.end(), std::copy(gridGeometry.begin(), gridGeometry.end(), gridGeometryColor.begin()));

	//this is the index array that provides the triangle coordinates needed to draw
	//the grid
	std::array<int, 384> gridTopology = GeometricTools::UnitGridTopologyTriangles<8, 8>();

	auto gridBufferLayout = BufferLayout({ {ShaderDataType::Float2, 0, "position"}, {ShaderDataType::Float3, sizeof(float) * gridGeometry.size(), "color"}});
	
	auto gridVertexBuffer = std::make_shared<VertexBuffer>(gridGeometryColor.data(), sizeof(gridGeometryColor));
	gridVertexBuffer->setLayout(gridBufferLayout);
	
	auto vertexArray = std::make_shared<VertexArray>();
	vertexArray->addVertexBuffer(gridVertexBuffer);
	
	auto gridIndexBuffer = std::make_shared<IndexBuffer>(gridTopology.data(), gridTopology.size());
	
	vertexArray->setIndexBuffer(gridIndexBuffer);
	
	Shader shaderProgram(vertexShaderSrc, fragmentShaderSrc);
	shaderProgram.bind();

	//sets background color
	glClearColor(0.5f, 0.0f, 0.0f, 1.0f);

	bool black = true;
	
	while (!glfwWindowShouldClose(GLFWApplication::window)) {

		//the key callback function, the function that processes the key inputs
		glfwSetKeyCallback(GLFWApplication::window, key_callback);
		//this piece of code is inserted to make sure that each and every single
		//input is processed
		glfwSetInputMode(GLFWApplication::window, GLFW_STICKY_KEYS, 1);
		glfwWaitEvents(); //this is to be used when the screen is to be updated
						  //only when inputs are given
		//glfwPollEvents();

		//clears the window at every frame before rendering
		glClear(GL_COLOR_BUFFER_BIT);	
		
		gridVertexBuffer->BufferSubData(gridGeometry.size() * sizeof(float), boardColor.size() * sizeof(float), &greenSelector(boardColor));

		glDrawElements(GL_TRIANGLES, 384, GL_UNSIGNED_INT, (void*)0);
		
		//renders the window
		glfwSwapBuffers(GLFWApplication::window);

		if (glfwGetKey(GLFWApplication::window, GLFW_KEY_ESCAPE) == GLFW_PRESS) break;

	}

	glfwDestroyWindow(GLFWApplication::window);
	glfwTerminate();
	return EXIT_SUCCESS;

};

//this is the function that moves the tile selector on the board
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	//the selector moves when a key is either pressed or kept held down
	if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		//these ifs statements are there to make sure the tile selector cannot
		//exit the chess board
		if (gGridColumn < 7) gGridColumn++;
	}
	if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		if (gGridColumn > 0 ) gGridColumn--;
	}
	if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		if (gGridRow > 0) gGridRow--;
	}
	if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		if (gGridRow < 7) gGridRow++;
	}
}

array<float, 768> greenSelector(array<float, 768> color) {

	int index = gGridRow * 32 * 3;

	index += gGridColumn * 6;

	    color[index] = 0.0f;
	color[index + 1] = 1.0f;
	color[index + 2] = 0.0f;

	color[index + 3] = 0.0f;
	color[index + 4] = 1.0f;
	color[index + 5] = 0.0f;

	index += 16 * 3;

		color[index] = 0.0f;
	color[index + 1] = 1.0f;
	color[index + 2] = 0.0f;

	color[index + 3] = 0.0f;
	color[index + 4] = 1.0f;
	color[index + 5] = 0.0f;

	return color;

}
