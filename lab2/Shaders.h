#ifndef SHADERS_H
#define SHADERS_H

#include <string>

const string vertexShaderSrc = R"(
	#version 460 core

	layout(location = 0) in vec2 position;
	layout(location = 1) in vec3 color;

	out vec3 ourColor;

	void main(){

	gl_Position = vec4(position, 0.0, 1.0);
	ourColor = color;

	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
const string fragmentShaderSrc = R"(
	#version 460 core
		
	in vec3 ourColor;

	out vec4 colorFinal;
	void main(){

	colorFinal = vec4(ourColor, 1.0); 

	}
	)";

#endif