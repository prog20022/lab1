#ifndef LAB2APPLICATION_H
#define LAB2APPLICATION_H

#include <GLFWApplication.h>
#include <GeometricTools.h>
#include <Rendering.h>

class Lab2Application : virtual public GLFWApplication {

public:
	Lab2Application(const char* name, string version) : GLFWApplication(name,version) {};
	unsigned int parseArguments(int argc, char** argv);
	unsigned init() {
		GLFWApplication::init();
		return EXIT_SUCCESS;
	};
	unsigned run();
	
};

#endif