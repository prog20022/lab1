#ifndef SHADERS_H
#define SHADERS_H

#include <string>

const string vertexShaderSrc1 = R"(
	#version 460 core

	layout(location = 0) in vec2 position;
	layout(location = 1) in vec3 color;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	out vec3 ourColor;

	void main(){

	gl_Position = projection * view * model * vec4(position,0.0, 1.0);
	ourColor = color;

	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
const string fragmentShaderSrc1 = R"(
	#version 460 core
		
	in vec3 ourColor;

	out vec4 colorFinal;
	void main(){

	colorFinal = vec4(ourColor, 1.0); 

	}
	)";

const string vertexShaderSrc2 = R"(
	#version 460 core

	layout(location = 0) in vec3 position;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	void main(){

	gl_Position = projection * view * model * vec4(position, 1.0);
	
	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
const string fragmentShaderSrc2 = R"(
	#version 460 core
		
	uniform vec3 ourColor;

	out vec4 colorFinal;
	void main(){

	colorFinal = vec4(ourColor, 1.0); 

	}
	)";

#endif