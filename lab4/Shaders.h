#ifndef SHADERS_H
#define SHADERS_H

#include <string>

const string vertexShaderSrc1 = R"(
	#version 460 core

	layout(location = 0) in vec2 position;
	layout(location = 1) in vec3 color;
	layout(location = 2) in vec2 tcoords;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	out vec3 ourColor;
	out vec2 vs_tcoords;

	void main(){

	gl_Position = projection * view * model * vec4(position,0.0, 1.0);
	ourColor = color;
	vs_tcoords = tcoords;
	ourColor = color;

	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
//colorFinal = vec4(ourColor, 1.0);
const string fragmentShaderSrc1 = R"(
	#version 460 core
		
	in vec3 ourColor;
	in vec2 vs_tcoords;

	layout(binding=0) uniform sampler2D u_floorTextureSampler;

	out vec4 colorFinal;
	void main(){

	colorFinal = mix(vec4(ourColor, 1.0f), texture(u_floorTextureSampler, vs_tcoords), 0.7f); 

	}
	)";

const string vertexShaderSrc2 = R"(
	#version 460 core

	layout(location = 0) in vec3 position;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	out vec3 vs_position;

	void main(){

	gl_Position = projection * view * model * vec4(position, 1.0);
	vs_position = position;	

	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
//colorFinale = vec4()
const string fragmentShaderSrc2 = R"(
	#version 460 core

	layout(binding=1) uniform samplerCube uTexture;

	uniform vec3 ourColor;		

	in vec3 vs_position;

	out vec4 colorFinal;
	void main(){
	
	if (ourColor == vec3(1.0f, 1.0f, 0.0f) || ourColor == vec3(0.0f, 1.0f, 0.0f) ||
		ourColor == vec3(0.92f, 0.92f, 1.0f)){
		colorFinal = mix(vec4(ourColor, 1.0f), texture(uTexture, vs_position), 0.5f);
	} else {
		colorFinal = mix(vec4(ourColor, 1.0f), texture(uTexture, vs_position), 1.0f);
	}
	colorFinal = vec4(colorFinal.xyz, 0.5f);
	 
	
	}
	)";

#endif