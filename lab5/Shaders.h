#ifndef SHADERS_H
#define SHADERS_H

#include <string>

const string vertexShaderSrc1 = R"(
	#version 460 core

	layout(location = 0) in vec2 position;
	layout(location = 1) in vec3 color;
	layout(location = 2) in vec2 tcoords;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	out vec3 ourColor;
	out vec2 vs_tcoords;

	void main(){

	gl_Position = projection * view * model * vec4(position,0.0, 1.0);
	ourColor = color;
	vs_tcoords = tcoords;
	ourColor = color;

	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
//colorFinal = vec4(ourColor, 1.0);
const string fragmentShaderSrc1 = R"(
	#version 460 core
		
	uniform float u_ambientStrength = 1.0;

	in vec3 ourColor;
	in vec2 vs_tcoords;

	layout(binding=0) uniform sampler2D u_floorTextureSampler;

	out vec4 colorFinal;
	void main(){

	colorFinal = u_ambientStrength * mix(vec4(ourColor, 1.0f), texture(u_floorTextureSampler, vs_tcoords), 0.7f); 

	}
	)";

const string vertexShaderSrc2 = R"(
	#version 460 core

	layout(location = 0) in vec3 position;
	layout(location = 1) in vec3 i_normal;	

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	out vec3 vs_position;
	out vec4 vs_normal;
	out vec4 vs_fragPosition;

	void main(){

	gl_Position = projection * view * model * vec4(position, 1.0);
	vs_position = position;
	vs_normal = normalize(model * vec4(i_normal, 1.0f));
	vs_fragPosition = model * vec4(position, 1.0f);	

	}
	)";

//the code for the fragment shader (a fragment shader is the fill and color of the shape in the GPU)
//for here, uniform has been used to draw the triangles
//uniform vec4 ourColor;
//colorFinale = vec4()
const string fragmentShaderSrc2 = R"(
	#version 460 core

	layout(binding=1) uniform samplerCube uTexture;

	uniform vec3 ourColor;
	uniform float u_ambientStrength = 1.0f;
	uniform vec3 u_lightSourcePosition;
	uniform float u_diffuseStrength;
	uniform vec3 u_cameraPosition;
	uniform float u_specularStrength;		

	in vec3 vs_position;
	in vec4 vs_normal;
	in vec4 vs_fragPosition;

	out vec4 colorFinal;

	void main(){
	
	if (ourColor == vec3(1.0f, 1.0f, 0.0f) || ourColor == vec3(0.0f, 1.0f, 0.0f) ||
		ourColor == vec3(0.92f, 0.92f, 1.0f)){
		colorFinal = mix(vec4(ourColor, 1.0f), texture(uTexture, vs_position), 1.0f);
	} else {
		colorFinal = mix(vec4(ourColor, 1.0f), texture(uTexture, vs_position), 1.0f);
	}
	vec3 lightDirection = normalize(vec3(u_lightSourcePosition - vs_fragPosition.xyz));
    float diffuseStrength = max(dot(lightDirection, vs_normal.xyz), 0.0f) * u_diffuseStrength;

	vec3 reflectedLight = normalize(reflect(lightDirection, vs_normal.xyz));
	vec3 observerDirection = normalize(u_cameraPosition - vs_fragPosition.xyz);
	float specFactor = pow(max(dot(observerDirection, reflectedLight), 0.0f), 32.0f);
	float specular = specFactor * u_specularStrength;

	colorFinal = (u_ambientStrength + diffuseStrength + specular) * vec4(colorFinal.xyz, 1.0f);
	
	}
	)";

#endif