#include "Lab2Application.h"

using namespace std;

int main(int argc, char* argv[]) {

	Lab2Application test("Hello", "2.0");
	
	test.parseArguments(argc, argv);

	test.init();

	return test.run();

}
