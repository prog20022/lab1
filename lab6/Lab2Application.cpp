#include "Lab2Application.h"
#include "Shaders.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <stb_image.h>
#include <algorithm>
#include <iostream>
#include <string>

/*
*
* This program here draws a chessboard. 
* 
* This program uses a separate draw call for each square of the chess board
* 
* This is not very efficient, but it worked and at this level, performance
* is not as important
* 
*/



//this global variable is where in the grid that the
//tile selector is position. On which square it is
//it is a global variable so that the key_input function can update
//it and hence move the tile selector on the screen
int gGridRow = 4;
int gGridColumn = 4;
bool gRotateLeft = false;
bool gRotateRight = false;
bool gRotateUp = false;
bool gRotateDown = false;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

array<float, 768> greenSelector(array<float, 768> color);

unsigned int Lab2Application::parseArguments(int argc, char** argv) {
	
	//creating our command line attributes
	try {
		
		TCLAP::CmdLine cmd("", ' ', GLFWApplication::version, false);
		
		TCLAP::ValueArg<int> heightArg("h", "height", "Height of the window", false, 800, "int");
		cmd.add(heightArg);

		TCLAP::ValueArg<int> widthArg("w", "width", "Width of the window", false, 800, "int");
		cmd.add(widthArg);
		
		// Parse the argv array.
		cmd.parse(argc, argv);

		Lab2Application::height = heightArg.getValue();
		Lab2Application::width = widthArg.getValue();

		//ensuring positive values in the argument
		if (height < 1 || width < 1) {
			cout << "The height and width parameters have to be 1 or greater." << endl;
			cin.get();
			return EXIT_FAILURE;
		}
		
	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}

	return EXIT_SUCCESS;
};

unsigned Lab2Application::run() {
	
	TextureManager* textureManager = TextureManager::getInstace();

	textureManager->loadTexture2DRGBA("floor", std::string(TEXTURES_DIR) + std::string("floor_texture.png"), 0);
	textureManager->loadCubeMapRGBA("cube", std::string(TEXTURES_DIR) + std::string("cube_texture.png"), 1);


	//we create our grid vertices
	std::array<float, 512> gridGeometry = GeometricTools::UnitGridGeometry2D<8, 8>();
	
	std::array<float, 768> boardColor = GeometricTools::chessBoardColorSetup<8, 8>();

	std::array<float, 512> gridTexture = GeometricTools::UnitGridGeometry2DTexture<8, 8>();

	std::array<float, gridGeometry.size() + boardColor.size()> gridGeometryColor;
	std::copy(boardColor.begin(), boardColor.end(), std::copy(gridGeometry.begin(), gridGeometry.end(), gridGeometryColor.begin()));
	std::array<float, gridGeometryColor.size() + gridTexture.size()> gridGeometryColorTexture;
	std::copy(gridTexture.begin(), gridTexture.end(), std::copy(gridGeometryColor.begin(), gridGeometryColor.end(), gridGeometryColorTexture.begin()));

	//this is the index array that provides the triangle coordinates needed to draw
	//the grid
	std::array<int, 384> gridTopology = GeometricTools::UnitGridTopologyTriangles<8, 8>();

	auto gridBufferLayout = BufferLayout({ {ShaderDataType::Float2, 0, "position"},
										   {ShaderDataType::Float3, sizeof(float) * gridGeometry.size(), "color"},
										   {ShaderDataType::Float2, sizeof(float) * gridGeometryColor.size(), "texture"} });
	
	auto gridVertexBuffer = std::make_shared<VertexBuffer>(gridGeometryColorTexture.data(), sizeof(gridGeometryColorTexture));
	gridVertexBuffer->setLayout(gridBufferLayout);
	
	auto vertexArray = std::make_shared<VertexArray>();
	vertexArray->addVertexBuffer(gridVertexBuffer);
	
	auto gridIndexBuffer = std::make_shared<IndexBuffer>(gridTopology.data(), gridTopology.size());
	
	vertexArray->setIndexBuffer(gridIndexBuffer);
	glm::vec3 cameraPosition = glm::vec3(0, 0, 5);
	PerspectiveCamera camera({45.0f, 1.0f, 1.0f, 1.0f, -10.0f}, glm::vec3(5,5,5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//OrthographicCamera camera({ -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f }, glm::vec3(0.5f, 0.0f,0.0f), 45.0f);

	auto chessboardModelMatrix = glm::mat4(1.0f);
	auto chessboardRotation = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	auto chessboardTranslation = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	auto chessboardScale = glm::scale(glm::mat4(1.0f), glm::vec3(8.0f, 8.0f, 8.0f));
	chessboardModelMatrix = chessboardTranslation * chessboardRotation * chessboardScale;
	
	Shader shaderProgram1(vertexShaderSrc1, fragmentShaderSrc1);
	shaderProgram1.bind();
	shaderProgram1.uploadUniformMat4("model", chessboardModelMatrix);
	shaderProgram1.uploadUniformMat4("view", camera.getViewMatrix());
	shaderProgram1.uploadUniformMat4("projection", camera.getProjectionMatrix());
	glm::vec3 lightColor = glm::vec3(0.75f, 0.25f, 0.75f);
	//glm::vec3 lightColor = glm::vec3(0.0f, 1.0f, 0.0f);
	shaderProgram1.uploadUniformFloat3("u_lightColor", lightColor);

	std::array<GLfloat, 72> cube = GeometricTools::UnitCubeIllumination;
	std::array<GLfloat, 72> cubeNormals = GeometricTools::UnitCubeIlluminationNormals;
	std::array<GLfloat, cube.size() + cubeNormals.size()> cubeData;
	std::copy(cubeNormals.begin(), cubeNormals.end(), std::copy(cube.begin(), cube.end(), cubeData.begin()));
	array<GLint, 36> cubeIndex = GeometricTools::UnitCubeTopologyIllumination;

	auto cubeBufferLayout = BufferLayout({ {ShaderDataType::Float3, 0, "position"},
										   {ShaderDataType::Float3, sizeof(float) * cube.size(), "position"} });

	auto cubeVertexBuffer = std::make_shared<VertexBuffer>(cubeData.data(), sizeof(cubeData));
	cubeVertexBuffer->setLayout(cubeBufferLayout);

	auto cubeVertexArray = std::make_shared<VertexArray>();
	cubeVertexArray->addVertexBuffer(cubeVertexBuffer);

	auto cubeIndexBuffer = std::make_shared<IndexBuffer>(cubeIndex.data(), sizeof(cubeIndex));

	cubeVertexArray->setIndexBuffer(cubeIndexBuffer);

	cubeVertexArray->bind();

	auto cubeModelMatrix = glm::mat4(1.0f);
	auto cubeScale = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	cubeModelMatrix = cubeScale * cubeModelMatrix;

	Shader shaderProgram2(vertexShaderSrc2, fragmentShaderSrc2);
	shaderProgram2.bind();
	shaderProgram2.uploadUniformMat4("model", cubeModelMatrix);
	shaderProgram2.uploadUniformMat4("view", camera.getViewMatrix());
	shaderProgram2.uploadUniformMat4("projection", camera.getProjectionMatrix());
	shaderProgram2.uploadUniformFloat3("ourColor", glm::vec3(1.0f, 1.0f, 1.0f));
	shaderProgram2.uploadUniformFloat3("u_lightColor", lightColor);
	shaderProgram2.uploadUniformFloat3("u_lightSourcePosition", cameraPosition);
	shaderProgram2.uploadUniformFloat1("u_diffuseStrength", 0.5f);
	shaderProgram2.uploadUniformFloat3("u_cameraPosition", glm::vec3(5, 5, 5));
	shaderProgram2.uploadUniformFloat1("u_specularStrength", 0.5f);

	//sets background color
	float ambientStrength = 0.9f;
	glm::vec3 backgroundColor = glm::vec3(0.5f, 0.0f, 0.0f);
	glm::vec3 ambient;
	glm::vec3 result;
	glm::vec4 finalBackgroundColor;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	bool black = true;
	
	while (!glfwWindowShouldClose(GLFWApplication::window)) {

		//the key callback function, the function that processes the key inputs
		glfwSetKeyCallback(GLFWApplication::window, key_callback);
		//this piece of code is inserted to make sure that each and every single
		//input is processed
		glfwSetInputMode(GLFWApplication::window, GLFW_STICKY_KEYS, 1);
		//glfwWaitEvents(); //this is to be used when the screen is to be updated
						  //only when inputs are given
		glfwPollEvents();

		//clears the window at every frame before rendering
		RenderCommands::clear();

		float timeValue = glfwGetTime();
		ambientStrength = sin(timeValue) / 2.0f + 0.5f;
		ambient = ambientStrength * lightColor;
		result = ambient * backgroundColor;
		finalBackgroundColor = glm::vec4(result, 1.0f);
		RenderCommands::setClearColor(finalBackgroundColor);
		
		vertexArray->bind();
		shaderProgram1.bind();
		shaderProgram1.uploadUniformFloat1("u_ambientStrength", ambientStrength);
		RenderCommands::setSolidMode();
		gridVertexBuffer->BufferSubData(gridGeometry.size() * sizeof(float), boardColor.size() * sizeof(float), &greenSelector(boardColor));
		RenderCommands::drawIndex(vertexArray, GL_TRIANGLES);

		if (gRotateUp) {
			auto chessboardRotate = glm::rotate(glm::mat4(1.0f), glm::radians(1.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
			cubeModelMatrix = chessboardRotate * cubeModelMatrix;
			shaderProgram2.uploadUniformMat4("model", cubeModelMatrix);
			gRotateUp = false;
		}

		if (gRotateDown) {
			auto chessboardRotate = glm::rotate(glm::mat4(1.0f), glm::radians(1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
			cubeModelMatrix = chessboardRotate * cubeModelMatrix;
			shaderProgram2.uploadUniformMat4("model", cubeModelMatrix);
			gRotateDown = false;
		}

		if (gRotateLeft) {
			auto chessboardRotate = glm::rotate(glm::mat4(1.0f), glm::radians(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
			cubeModelMatrix = chessboardRotate * cubeModelMatrix;
			shaderProgram2.uploadUniformMat4("model", cubeModelMatrix);
			gRotateLeft = false;
		}

		if (gRotateRight) {
			auto chessboardRotate = glm::rotate(glm::mat4(1.0f), glm::radians(1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			cubeModelMatrix = chessboardRotate * cubeModelMatrix;
			shaderProgram2.uploadUniformMat4("model", cubeModelMatrix);
			gRotateRight = false;
		}
		
		cubeVertexArray->bind();
		shaderProgram2.bind();
		shaderProgram2.uploadUniformFloat1("u_ambientStrength", ambientStrength);
		RenderCommands::setSolidMode();
		RenderCommands::drawIndex(cubeVertexArray, GL_TRIANGLES);
		
		
		if (glfwGetKey(GLFWApplication::window, GLFW_KEY_1) == GLFW_PRESS) {
			shaderProgram2.uploadUniformFloat3("ourColor", glm::vec3(1.0f, 1.0f, 0.0f));
		}
		if (glfwGetKey(GLFWApplication::window, GLFW_KEY_2) == GLFW_PRESS) {
			shaderProgram2.uploadUniformFloat3("ourColor", glm::vec3(0.0f, 1.0f, 0.0f));
		}
		if (glfwGetKey(GLFWApplication::window, GLFW_KEY_3) == GLFW_PRESS) {
			shaderProgram2.uploadUniformFloat3("ourColor", glm::vec3(0.92f, 0.92f, 1.0f));
		}

		//renders the window
		glfwSwapBuffers(GLFWApplication::window);

		if (glfwGetKey(GLFWApplication::window, GLFW_KEY_ESCAPE) == GLFW_PRESS) break;

	}

	glfwDestroyWindow(GLFWApplication::window);
	glfwTerminate();
	return EXIT_SUCCESS;

};

//this is the function that moves the tile selector on the board
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	//the selector moves when a key is either pressed or kept held down
	if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		//these ifs statements are there to make sure the tile selector cannot
		//exit the chess board
		if (gGridColumn < 7) gGridColumn++;
	}
	if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		if (gGridColumn > 0 ) gGridColumn--;
	}
	if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		if (gGridRow > 0) gGridRow--;
	}
	if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		if (gGridRow < 7) gGridRow++;
	}
	if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		gRotateUp = true;
	}
	if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		gRotateDown = true;
	}
	if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		gRotateLeft = true;
	}
	if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
		gRotateRight = true;
	}
}

array<float, 768> greenSelector(array<float, 768> color) {

	int index = gGridRow * 32 * 3;

	index += gGridColumn * 6;

	    color[index] = 0.0f;
	color[index + 1] = 1.0f;
	color[index + 2] = 0.0f;

	color[index + 3] = 0.0f;
	color[index + 4] = 1.0f;
	color[index + 5] = 0.0f;

	index += 16 * 3;

		color[index] = 0.0f;
	color[index + 1] = 1.0f;
	color[index + 2] = 0.0f;

	color[index + 3] = 0.0f;
	color[index + 4] = 1.0f;
	color[index + 5] = 0.0f;

	return color;

}
